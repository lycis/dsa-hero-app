export default function contract (definition, obj) {
  let contractObj = { _ref: obj }
  for (let k of Object.keys(definition)) {
    let t = typeof obj[k]
    if (t !== 'function') {
      throw new Error(`interface contract violated (function '${k}' === ${t})`)
    } else {
      contractObj[k] = function (...args) { return this._ref[k](...args) }
    }
  }
  return contractObj
}
