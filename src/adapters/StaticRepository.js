import axios from 'axios'
import HeroXMLLoader from '@/adapters/HeroLoaders'
var parseString = require('xml2js').parseString

export default class StaticRepository {
  constructor () {
    this.route = '/static/'
  }

  getHero (id) {
    return axios.get(`${this.route}/heroes/${id}.xml`).then(response => {
      let parsedHero = {}
      parseString(response.data, function (_, result) {
        let loader = new HeroXMLLoader(result.helden.held[0])
        parsedHero = loader.createHero()
      })
      return parsedHero
    }
    ).catch(error => { throw new Error(error) })
  }
}
