import Hero from '@/domain/hero'
import Eigenschaft from '@/domain/eigenschaft'

export default class HeroXMLLoader {
  constructor (data) {
    this._raw = data
  }

  createHero () {
    let hero = new Hero()
    hero.name = this._raw.$.name

    this._initEigenschaften(hero)
    this._initInformation(hero)

    return hero
  }

  _initEigenschaften (hero) {
    this._raw.eigenschaften[0].eigenschaft.forEach(e => {
      if (['Mut', 'Klugheit', 'Intuition', 'Charisma', 'Fingerfertigkeit', 'Gewandtheit', 'Konstitution', 'Körperkraft'].includes(e.$.name)) {
        hero.attributes[e.$.name] = new Eigenschaft(e.$.name, e.$.mod, e.$.startwert)
      } else if (['Lebensenergie', 'Ausdauer', 'Astralenergie'].includes(e.$.name)) {
        hero.basiswerte[e.$.name].modifikator = parseInt(e.$.mod)
        hero.basiswerte[e.$.name].gekauft = parseInt(e.$.value)
      } else if (e.$.name === 'Karmaenergie') {
        hero.basiswerte.Karmaenergie.basis = e.$.value
      }
    })
  }

  _initInformation (hero) {
    hero.gender = this._raw.basis[0].geschlecht[0].$.name
    hero.race = this._raw.basis[0].rasse[0].$.string
    hero.culture = this._raw.basis[0].kultur[0].$.string

    this._raw.basis[0].ausbildungen.forEach(ausbildung => hero.professions.push(ausbildung.ausbildung[0].$.string))

    let bmonth = ['PRA', 'RON', 'EFF', 'TRA', 'BOR', 'HES', 'FIR', 'TSA', 'PHE', 'PER', 'ING', 'RAH', 'NL'][this._raw.basis[0].rasse[0].aussehen[0].$.gbmonat - 1]
    hero.birthday = this._raw.basis[0].rasse[0].aussehen[0].$.gbtag + '. ' + bmonth + ' ' + this._raw.basis[0].rasse[0].aussehen[0].$.gbjahr + ' BF'

    hero.socialClass = this._raw.basis[0].rasse[0].aussehen[0].$.stand
    hero.title = this._raw.basis[0].rasse[0].aussehen[0].$.titel

    hero.size = this._raw.basis[0].rasse[0].groesse[0].$.value
    hero.weight = this._raw.basis[0].rasse[0].groesse[0].$.gewicht
    hero.eyeColor = this._raw.basis[0].rasse[0].aussehen[0].$.augenfarbe
    hero.hairColor = this._raw.basis[0].rasse[0].aussehen[0].$.haarfarbe

    hero.appearance = []
    hero.family = []
    Object.keys(this._raw.basis[0].rasse[0].aussehen[0].$).forEach(k => {
      let v = this._raw.basis[0].rasse[0].aussehen[0].$[k]
      if (k.startsWith('familietext') && v.length > 0) {
        hero.family.push(v)
      } else if (k.startsWith('aussehentext') && v.length > 0) {
        hero.appearance.push(v)
      }
    })
  }
}
