import Vue from 'vue'
import Router from 'vue-router'
import HomeView from '@/components/HomeView'
import HeroSheet from '@/components/HeroSheet'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/hero_sheet/:id',
      name: 'hero_sheet',
      component: HeroSheet
    }
  ]
})
