export default class Eigenschaft {
  constructor (name, mod, start) {
    this.name = name
    this.mod = parseInt(mod)
    this.start = parseInt(start)
  }

  get current () {
    return this.mod + this.start
  }

  get modifier () {
    return this.mod
  }

  get baseValue () {
    return this.baseValue
  }
}
