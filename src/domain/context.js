import {Repository} from './interfaces'
import contract from '@/contract'

export class BaseContext {
  constructor (repository) {
    this.repository = contract(Repository, repository)
  }
}

export class HeroContext extends BaseContext {
  async queryInformation (id) {
    return this.repository.getHero(id).then(hero => {
      let info = {
        grundwerte: {
          geschlecht: hero.gender,
          rasse: hero.race,
          kultur: hero.culture,
          ausbildung: hero.professions,
          geburtstag: hero.birthday,
          stand: hero.socialClass,
          titel: hero.title
        },
        aussehen: {
          groesse: hero.size,
          gewicht: hero.weight,
          augenfarbe: hero.eyeColor,
          haarfarbe: hero.hairColor,
          beschreibung: hero.appearance
        },
        familie: hero.family
      }
      return info
    })
  }

  async queryAttributes (id) {
    return this.repository.getHero(id).then(hero => { return hero.attributes })
  }
}
