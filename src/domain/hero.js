import Eigenschaft from '@/domain/eigenschaft'

export default class Hero {
  constructor (data) {
    this.attributes = {
      Mut: new Eigenschaft('Mut', 0, 0),
      Klugheit: new Eigenschaft('Klugheit', 0, 0),
      Intuition: new Eigenschaft('Intuition', 0, 0),
      Charisma: new Eigenschaft('Charisma', 0, 0),
      Fingerfertigkeit: new Eigenschaft('Fingerfertigkeit', 0, 0),
      Gewandtheit: new Eigenschaft('Gewandtheit', 0, 0),
      Konstitution: new Eigenschaft('Konstitution', 0, 0),
      Körperkraft: new Eigenschaft('Körperkraft', 0, 0)
    }
    this.basiswerte = {
      Ausdauer: { modifikator: 0, gekauft: 0 },
      Lebensenergie: { modificator: 0, gekauft: 0 },
      Astralenergie: { modificator: 0, gekauft: 0 },
      Karmaenergie: { modificator: 0, gekauft: 0 }
    }
    this.gender = ''
    this.race = ''
    this.culture = ''
    this.professions = []
    this.birthday = ''
    this.socialClass = ''
    this.title = ''
    this.size = 0
    this.weight = 0
    this.eyeColor = ''
    this.hairColor = ''
    this.appearance = []
    this.family = []
  }

  get totalHitpoints () {
    return (
      Math.round((this.attributes.Konstitution.current * 2 + this.attributes.Körperkraft.current) / 2) +
      this.basiswerte.Lebensenergie.modifikator +
      this.basiswerte.Lebensenergie.gekauft
    )
  }

  get totalEndurance () {
    return (
      Math.round((this.attributes.Mut.current + this.attributes.Konstitution.current + this.attributes.Gewandtheit.current) / 2) +
      this.basiswerte.Ausdauer.modifikator +
      this.basiswerte.Ausdauer.gekauft
    )
  }

  get totalAstralEnergy () {
    return (
      Math.round((this.attributes.Mut.current + this.attributes.Intuition.current + this.attributes.Charisma.current) / 2) +
      this.basiswerte.Astralenergie.modifikator +
      this.basiswerte.Astralenergie.gekauft
    )
  }

  get totalKarma () {
    return this.basiswerte.Karmaenergie.basis
  }
}
