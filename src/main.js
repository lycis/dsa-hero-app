// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Quasar, { QLayout, QInput, QBtn, QToolbar, QToolbarTitle, QCard, QCardTitle, QCardMain, QCardMedia, QCardSeparator, QCardActions, QTabs, QTab, QTabPane, QRouteTab, QSpinner, QCollapsible, QPageContainer, QLayoutHeader} from 'quasar-framework/dist/quasar.mat.esm'
import 'quasar-framework/dist/umd/quasar.mat.css'
import 'quasar-extras/material-icons/material-icons.css'
import { HeroContext } from '@/domain/context'
import StaticRepository from '@/adapters/StaticRepository'

Vue.config.productionTip = false
Vue.use(Quasar, { components: [
  QLayout, QInput, QBtn, QToolbar, QToolbarTitle,
  QCard,
  QCardTitle,
  QCardMain,
  QCardMedia,
  QCardSeparator,
  QCardActions,
  QTabs, QTab, QTabPane, QRouteTab,
  QSpinner,
  QCollapsible,
  QPageContainer,
  QLayoutHeader
] })

Vue.prototype.$context = {
  hero: new HeroContext(new StaticRepository())
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
