import Vue from 'vue'
import Info from '@/components/sheet/Info.vue'
import Hero from '@/domain/hero'
import { HeroContext } from '@/domain/context'
import { mount } from '@vue/test-utils'
import Quasar, { QTabs, QTab, QTabPane, QSpinner, QCard, QCardMain, QCardTitle } from 'quasar-framework/dist/quasar.mat.esm'
import flushPromises from 'flush-promises'

Vue.use(Quasar, { components: [
  QTabs, QTab, QTabPane,
  QSpinner,
  QCard, QCardMain, QCardTitle
] })

function mockHero () {
  let hero = new Hero()
  hero.gender = 'weiblich'
  hero.race = 'Waldelf'
  hero.culture = 'Garetien'
  hero.professions = ['Legendensänger', 'Fischer']
  hero.birthday = '1. HES 453 BF'
  hero.socialClass = 'Adel'
  hero.title = 'Herzog'
  hero.size = 167
  hero.weight = 55
  hero.eyeColor = 'bernstein'
  hero.hairColor = 'blond'
  hero.appearance = ['A', 'b']
  hero.family = ['1 Bruder', '27 Schwestern']
  return hero
}

function renderHeroInfo () {
  let repository = {
    getHero: async function (id) {
      return mockHero()
    }
  }
  Vue.prototype.$context = {hero: new HeroContext(repository)}
  let wrapper = mount(Info, {propsData: { heroId: 5 }})
  return wrapper
}

describe('Info.vue', async () => {
  it('renders an error when hero data cannot be fetched', async () => {
    let repository = {
      getHero: function (id) {
        return Promise.reject(new Error({response: {status: '400', statusText: 'hero not available'}}))
      }
    }
    Vue.prototype.$context = {hero: new HeroContext(repository)}

    let wrapper = mount(Info, {propsData: { heroId: 5 }})
    await flushPromises()
    expect(wrapper.find('#info-error-message').text()).to.equal('failed to load hero information')
  })

  it('should render the heros gender', async () => {
    let wrapper = renderHeroInfo()
    await flushPromises()
    expect(wrapper.find('#info-geschlecht').text()).to.equal('weiblich')
  })

  it('should render the heros race', async () => {
    let wrapper = renderHeroInfo()
    await flushPromises()
    expect(wrapper.find('#info-rasse').text()).to.equal('Waldelf')
  })

  it('should render the heros culture', async () => {
    let wrapper = renderHeroInfo()
    await flushPromises()
    expect(wrapper.find('#info-kultur').text()).to.equal('Garetien')
  })

  it('should render the heros culture', async () => {
    let wrapper = renderHeroInfo()
    await flushPromises()
    expect(wrapper.find('#info-kultur').text()).to.equal('Garetien')
  })
})
