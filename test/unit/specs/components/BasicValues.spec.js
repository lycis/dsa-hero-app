import Vue from 'vue'
import BasicValues from '@/components/sheet/BasicValues.vue'
import Hero from '@/domain/hero'
import Eigenschaft from '@/domain/eigenschaft'

function renderBasicValues () {
  let hero = new Hero()
  hero.attributes.Mut = new Eigenschaft('Mut', 0, 13)
  hero.attributes.Konstitution = new Eigenschaft('Konstitution', 0, 15)
  hero.attributes.Gewandtheit = new Eigenschaft('Gewandtheit', 0, 10)
  hero.attributes.Körperkraft = new Eigenschaft('Körperkraft', 0, 10)
  hero.attributes.Intuition = new Eigenschaft('Intuition', 2, 13)
  hero.attributes.Charisma = new Eigenschaft('Charisma', 0, 14)

  hero.basiswerte.Ausdauer = {modifikator: 10, gekauft: 1}
  hero.basiswerte.Lebensenergie = {modifikator: 10, gekauft: 5}
  hero.basiswerte.Astralenergie = {modifikator: 12, gekauft: 0}
  hero.basiswerte.Karmaenergie = {basis: 24}

  const Constructor = Vue.extend(BasicValues)
  const vm = new Constructor({propsData: { hero: hero }}).$mount()
  return vm
}

describe('BasicValues.vue', () => {
  it('should render total hitpoints', () => {
    let vm = renderBasicValues()
    expect(vm.$el.querySelector('#bv-hitpoints').textContent).to.equal('35')
  })

  it('should render total endurance', () => {
    let vm = renderBasicValues()
    expect(vm.$el.querySelector('#bv-endurance').textContent).to.equal('30')
  })

  it('should render total astral energy', () => {
    let vm = renderBasicValues()
    expect(vm.$el.querySelector('#bv-astralenergy').textContent).to.equal('33')
  })

  it('should render karma', () => {
    let vm = renderBasicValues()
    expect(vm.$el.querySelector('#bv-karma').textContent).to.equal('24')
  })
})
