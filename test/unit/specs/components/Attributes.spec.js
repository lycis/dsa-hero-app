import Hero from '@/domain/Hero'
import Eigenschaft from '@/domain/eigenschaft'
import Vue from 'vue'
import { mount } from '@vue/test-utils'
import { HeroContext } from '@/domain/context'
import Attributes from '@/components/sheet/Attributes'
import flushPromises from 'flush-promises'

function createDummyHero () {
  let hero = new Hero()
  hero.attributes = {
    Mut: new Eigenschaft('Mut', 0, 14),
    Klugheit: new Eigenschaft('Klugheit', 0, 13),
    Intuition: new Eigenschaft('Intuition', 0, 12),
    Charisma: new Eigenschaft('Charisma', 0, 14),
    Fingerfertigkeit: new Eigenschaft('Fingerfertigkeit', 0, 15),
    Gewandtheit: new Eigenschaft('Gewandtheit', 0, 15),
    Konstitution: new Eigenschaft('Konstitution', 0, 17),
    Körperkraft: new Eigenschaft('Körperkraft', 0, 9)
  }
  return hero
}

function renderAttributes () {
  let repository = {
    getHero: async function (id) {
      return createDummyHero()
    }
  }
  Vue.prototype.$context = {hero: new HeroContext(repository)}
  let wrapper = mount(Attributes, {propsData: { heroId: 5 }})
  return wrapper
}

describe('Attributes.vue', async () => {
  it('should render all attributes of the hero', async () => {
    let wrapper = renderAttributes()
    let refHero = createDummyHero()
    await flushPromises()
    Object.keys(refHero.attributes).forEach(element => {
      expect(wrapper.find(`#attribut-mod-${element}`).text()).to.equal(refHero.attributes[element].mod.toString())
      expect(wrapper.find(`#attribut-start-${element}`).text()).to.equal(refHero.attributes[element].start.toString())
      expect(wrapper.find(`#attribut-aktuell-${element}`).text()).to.equal(refHero.attributes[element].current.toString())
    })
  })
})
