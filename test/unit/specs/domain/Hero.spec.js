import Hero from '@/domain/hero'
import Eigenschaft from '@/domain/eigenschaft'

describe('Hero', () => {
  it('calculates base endurance based on formula "(MU + KO + GE) / 2 "', () => {
    let hero = new Hero()
    hero.attributes.Mut = new Eigenschaft('Mut', 0, 12)
    hero.attributes.Konstitution = new Eigenschaft('Konstitution', 0, 15)
    hero.attributes.Gewandtheit = new Eigenschaft('Gewandtheit', 0, 10)
    hero.basiswerte.Ausdauer = {modifikator: 0, gekauft: 0}
    expect(hero.totalEndurance).to.equal(19)
  })

  it('caclulates total endurance by adding modificator and bought endurance to base endurance', () => {
    let hero = new Hero()
    hero.attributes.Mut = new Eigenschaft('Mut', 0, 12)
    hero.attributes.Konstitution = new Eigenschaft('Konstitution', 0, 15)
    hero.attributes.Gewandtheit = new Eigenschaft('Gewandtheit', 0, 10)
    hero.basiswerte.Ausdauer = {modifikator: 10, gekauft: 1}

    expect(hero.totalEndurance).to.equal(30)
  })

  it('calculates astral energy by (MU + IN + CH)/2 + base + modifier', () => {
    let hero = new Hero()
    hero.attributes.Mut = new Eigenschaft('Mut', 0, 13)
    hero.attributes.Intuition = new Eigenschaft('Intuition', 0, 13)
    hero.attributes.Charisma = new Eigenschaft('Charisma', 0, 14)
    hero.basiswerte.Astralenergie = {modifikator: 12, gekauft: 1}

    expect(hero.totalAstralEnergy).to.equal(33)
  })

  it('calculates karma based on the preset', () => {
    let hero = new Hero()
    hero.basiswerte.Karmaenergie = { basis: 24 }
    expect(hero.totalKarma).to.equal(24)
  })
})
