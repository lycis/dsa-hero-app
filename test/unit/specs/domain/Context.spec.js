import {HeroContext} from '@/domain/context'
import Hero from '@/domain/hero'
import Eigenschaft from '@/domain/eigenschaft'

describe('HeroContext', () => {
  it('provides hero information', () => {
    let hero = new Hero()
    hero.gender = 'weiblich'
    hero.race = 'Waldelf'
    hero.culture = 'Garetien'
    hero.professions = ['Legendensänger', 'Fischer']
    hero.birthday = '1. HES 453 BF'
    hero.socialClass = 'Adel'
    hero.title = 'Herzog'
    hero.size = 167
    hero.weight = 55
    hero.eyeColor = 'bernstein'
    hero.hairColor = 'blond'
    hero.appearance = ['A', 'b']
    hero.family = ['1 Bruder', '27 Schwestern']

    let repository = {
      getHero: async function (id) {
        return hero
      }
    }

    let domain = new HeroContext(repository)
    domain.queryInformation('5').then(info => {
      expect(info.grundwerte.geschlecht).to.equal(hero.gender)
      expect(info.grundwerte.rasse).to.equal(hero.race)
      expect(info.grundwerte.kultur).to.equal(hero.culture)
      expect(info.grundwerte.ausbildung).to.equal(hero.professions)
      expect(info.grundwerte.geburtstag).to.equal(hero.birthday)
      expect(info.grundwerte.stand).to.equal(hero.socialClass)
      expect(info.grundwerte.titel).to.equal(hero.title)

      expect(info.aussehen.groesse).to.equal(hero.size)
      expect(info.aussehen.gewicht).to.equal(hero.weight)
      expect(info.aussehen.augenfarbe).to.equal(hero.eyeColor)
      expect(info.aussehen.haarfarbe).to.equal(hero.hairColor)
      expect(info.aussehen.beschreibung).to.equal(hero.appearance)

      expect(info.familie).to.equal(hero.family)
    })
  })

  it('provides attributes of a hero', () => {
    let hero = new Hero()
    hero.attributes = {
      Mut: new Eigenschaft('Mut', 0, 14),
      Klugheit: new Eigenschaft('Klugheit', 0, 13),
      Intuition: new Eigenschaft('Intuition', 0, 12),
      Charisma: new Eigenschaft('Charisma', 0, 14),
      Fingerfertigkeit: new Eigenschaft('Fingerfertigkeit', 0, 15),
      Gewandtheit: new Eigenschaft('Gewandtheit', 0, 15),
      Konstitution: new Eigenschaft('Konstitution', 0, 17),
      Körperkraft: new Eigenschaft('Körperkraft', 0, 9)
    }

    let repository = {
      getHero: async function (id) {
        return hero
      }
    }

    let domain = new HeroContext(repository)
    domain.queryAttributes('5').then(attributes => {
      expect(attributes.length).to.equal(hero.attributes.length)
      Object.keys(attributes).forEach(attributeName => {
        expect(attributes[attributeName]).to.equal(hero.attributes[attributeName])
      })
    })
  })
})
