import Eigenschaft from '@/domain/eigenschaft'

describe('Eigenschaft', () => {
  it('calculates the actual value based on base value and modifier', () => {
    let eigenschaft = new Eigenschaft('Konstitution', 1, 14)
    expect(eigenschaft.current).to.equal(15)
  })
})
