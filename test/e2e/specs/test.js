// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'render base values for a character': function (browser) {
    // automatically uses dev Server port from /config.index.js
    // default: http://localhost:8080
    // see nightwatch.conf.js
    const devServer = browser.globals.devServerURL

    browser
      .url(devServer + '#/hero_sheet/5')
      .waitForElementVisible('#bv-astralenergy', 5000)
      .assert.containsText('#bv-astralenergy', '33')
      .end()
  }
}
